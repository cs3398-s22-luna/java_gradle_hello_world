package hello;

// on line change

import static org.junit.jupiter.api.Assertions.*;
// import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

// Starting class (3-4)

// Terry Tosh - A14 test comment
// Katia's A14 test comment

public class TestGreeter {

   private Greeter g = new Greeter();

   @Test
   @DisplayName("Test for Empty Name")
   public void testGreeterEmpty() 

   {
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello!");
   }

   @Test
   @DisplayName("Test for Ted")
   public void testGreeterTed() 

   {
      g.setName("Ted");
      assertEquals(g.getName(),"Ted");
      assertEquals(g.sayHello(),"Hello Ted!");
   }
   
   @Test
   @DisplayName("Test for assertFalse")
   public void testAssertFalseEmily() 

   {
      g.setName("");
      assertFalse(g.getName() == "Emily");
      assertFalse(g.sayHello() == "Hello Emily!");
   }


   @Test
   @DisplayName("Test for Julian")
   public void testGreeterJulian(){
       g.setName("Julian");
       assertEquals(g.getName(),"Julian");
       assertEquals(g.sayHello(),"Hello Julian!");
   }

   @Test
   @DisplayName("Test for Julian 2")
      public void testAssertFalseJulian(){
         g.setName("Julian");
         String testStr = "JJJ";
         assertFalse(testStr == g.getName()); 
      }
   


   @Test
   @DisplayName("Test for Name='Bigyan'")
   public void testGreeter() 
   {

      g.setName("Bigyan");
      assertEquals(g.getName(),"Bigyan");
      assertEquals(g.sayHello(),"Hello Bigyan!");
   }

   @Test
   @DisplayName("Test for Name='Not Bigyan'")
   public void testAssertFalseBigyan() 
   {

      g.setName("Bigyan");
      assertFalse( (g.getName() == "Bhandari"), "GetName does not output expected results.");
   }


   @Test
   @DisplayName("Test for Terry")
   public void testGreeterTerry()
   {
      g.setName("Terry");
      assertEquals(g.getName(),"Terry");
      assertEquals(g.sayHello(),"Hello Terry!");
   }

   @Test
   @DisplayName("Test for assertFalse")
   public void testAssertFalseTerry()
   {
      assertFalse(0 == 1);
   }

   @Test
   @DisplayName("Test for Name='Katia'")
   public void testGreeterKatia()
   {
       g.setName("Katia");
       assertEquals(g.getName(), "Katia");
       assertEquals(g.sayHello(), "Hello Katia!");
   }

   @Test
   @DisplayName("Test for assertFalse()")
   public void testAssertFalseKatia()
   {
       assertFalse((20 * 4 + 54) == (88 - 10 * 5));
   }


   @Test
   @DisplayName("Test for isSame")
   public void testIsSameEmily() 

   {
      String a = "abc";
      String b = "abc";
      assertSame(a,b);
   }
}
